const express = require('express')
const app = express()
const port = 3000

app.set('view engine', 'pug')
app.use('/static', express.static('public'))
app.get('/', function (req, res) {
    let friendlyMessage = process.env.HOSTNAME || req.query.cluster_identifier || "Missing cluster_identifier"
    res.render('index', { title: friendlyMessage, altText: friendlyMessage})
  })

app.listen(port, () => console.log(`Starting on ${port}, logging is for chumps`))